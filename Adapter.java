// Interface desejada (InterfaceGalinha)
class InterfaceGalinha {
  cacarejar() {}
}

// Classe alvo (Galinha)
class Galinha extends InterfaceGalinha {
  cacarejar() {
    console.log("Có, có!");
  }
}

// Classe existente (Pato)
class Pato {
  grasnar() {
    console.log("Quack...!");
  }
}

// Adaptador
class AdaptadorPato extends InterfaceGalinha {
  constructor(pato) {
    super();
    this.pato = pato;
  }

  cacarejar() {
    this.pato.grasnar();
  }
}

// Demonstração
class AdaptadorPatoDemo {
  run() {
    const pato = new Pato();
    const adaptador = new AdaptadorPato(pato);

    // Agora podemos usar o objeto Pato como se fosse um objeto Galinha
    console.log("Pato:");
    pato.grasnar();

    console.log("\nAdaptadorPato:");
    adaptador.cacarejar();
  }
}

// Executar a demonstração
const demo = new AdaptadorPatoDemo();
demo.run();
